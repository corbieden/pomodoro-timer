﻿using System;
using System.Threading;
using System.Timers;
using System.Windows;
using PomodoroTimer.Properties;
using Timer = System.Timers.Timer;

namespace PomodoroTimer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static Timer _timer;
        private static DateTime end = DateTime.Now;

        public MainWindow()
        {
            InitializeComponent();
            _timer = new Timer
            {
                Interval = 50,
                Enabled = false
            };
            _timer.Elapsed += TimerOnElapsed;

            timeTextBlock.Text = $"{Properties.Settings.Default.MainTimer}:00";
        }

        private TimeSpan TimeLeft()
        {
            return end - DateTime.Now;
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            Dispatcher.BeginInvoke(new Action(delegate()
            {
                timeTextBlock.Text = $"{TimeLeft().Minutes:00}:{TimeLeft().Seconds:00}";
            }));

            if (TimeLeft() < TimeSpan.Zero)
            {
                _timer.Stop();
            }

            if (!_timer.Enabled)
                timeTextBlock.Text = $"{Properties.Settings.Default.MainTimer}:00";
        }

        private void ExitMenuItemClick(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void SettingsMenu_Click(object sender, RoutedEventArgs e)
        {
            new Settings().Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
                end = DateTime.Now.AddMinutes(Properties.Settings.Default.MainTimer);
                _timer.Start();
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            while(_timer.Enabled)
                Thread.Sleep(20);
            timeTextBlock.Text = $"{Properties.Settings.Default.MainTimer}:00";

            if(_timer.Enabled)
                stopButton_Click(sender, e);
        }
    }
}
