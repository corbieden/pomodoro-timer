﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PomodoroTimer.Properties;
using static System.Int32;

namespace PomodoroTimer
{
    /// <summary>
    /// Логика взаимодействия для Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();

            WorkTimeText.Text = Properties.Settings.Default.MainTimer.ToString();

            ShortBreakText.Text = Properties.Settings.Default.ShortBreak.ToString();

            LongBreakText.Text = Properties.Settings.Default.LongBreak.ToString();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            int workTime = 25, 

                shortBreak = 5, 

                longBreak = 15;

            try
            {
                workTime = Parse(WorkTimeText.Text);

                shortBreak = Parse(ShortBreakText.Text);

                longBreak = Parse(LongBreakText.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            #region проверка введенных значений

            if (workTime > 60 || workTime < 10)
            {
                MessageBox.Show("Incorrect work time", "Error");

                return;
            }

            if (shortBreak > 20 || shortBreak < 1)
            {
                MessageBox.Show("Incorrect short break time", "Error");

                return;
            }

            if (longBreak > 60 || longBreak < 10)
            {
                MessageBox.Show("Incorrect long break time", "Error");

                return;
            }

            // короткий перерыв не может быть больше длинного
            if(shortBreak > longBreak)
                MessageBox.Show("Short break time cannot be more than long break time", "Error");

            #endregion


            #region сохраняем настройки

            Properties.Settings.Default.MainTimer = workTime;

            Properties.Settings.Default.ShortBreak = shortBreak;

            Properties.Settings.Default.LongBreak = longBreak;

            Properties.Settings.Default.Save();

            #endregion

            Close();
        }
    }
}
